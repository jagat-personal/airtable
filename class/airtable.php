<?php
/**
 * A Simple PHP Library for AirTable. It manage your records in airtable vai api call.
 *
 *
 * @author Jagat Krishna Prajapati
 * @see https://bitbucket.org/jagat-personal/airtable/
 * @version 0.1
 * @access public
 *
 * Usage:
 *
 * First Add API Key and API URL
 * public $api_key = 'Your API KEY';
 * public $api_url = 'Your API URL';
 *
 * $airtable = new Air_table();
 * $data = array( 'column 1' => 'value of column 1', 'column 2' => 'value of column 2' );
 * $airtable->table('table name')->add_record( $data );
 *
 */
class Air_table {
	public $api_key = 'Your API KEY';
	public $api_url = 'Your API URL';

	public $table;

	public function table( $tbl = null ) {
		if ( $tbl )
			$this->table = $tbl;
		return $this;
	}

	/**
	 * Air Table API to call CURL.
	 *
	 * @param  Array  $data Data to be Execute.
	 * @param  String $method Method to send data in CURL.
	 * @param  String $record_id Record ID to update/ deleter record.
	 * @return Json
	 */
	public function airtable_call( $data=null, $method, $record_id = null ) {

		if ( ! $this->table ) {
			die( 'No table Selected' );
		}

		if ( ! $method ) {
			die( 'Method not specified.' );
		}

		$url = $this->api_url . $this->table . '/';

		if ( 'PATCH' == $method || 'DELETE' == $method ) {
			if ( ! $record_id ) {
				die( "Required 'record id' to update or delete" );
			}
			$url .= $record_id . '/';
		}
		if ( ( 'POST' != $method ) && ( 'PUT' != $method ) && ( 'PATCH' != $method ) ) {
			if ( $data ) {
		   		$url .= '?' . http_build_query( $data );
			}
		}

		$headers = array();
		$headers[] = 'cache-control: no-cache';
		$headers[] = 'Authorization: Bearer ' . $this->api_key;
		$headers[] = 'Content-Type:application/json';
		if ( $data ) {
			$headers[] = 'Content-Length: ' . strlen( $data );
		}

		$curl = curl_init();
		curl_setopt_array(
			$curl, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_HTTPHEADER => $headers,
				CURLOPT_SSL_VERIFYPEER => false,
			)
		);

		if ( 'POST' == $method || 'PATCH' == $method ) {
			curl_setopt( $curl, CURLOPT_POST, true );
			curl_setopt( $curl, CURLOPT_POSTFIELDS, $data );
			if ( 'POST' == $method ) {
				curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, 'POST' );
			} else {
				curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, 'PATCH' );
			}
		} elseif ( 'GET' == $method ) {
			curl_setopt( $curl, CURLOPT_POST, 0 );
			curl_setopt( $curl, CURLOPT_HTTPGET, true );
			curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, 'GET' );
		} elseif ( 'DELETE' == $method ) {
			curl_setopt( $curl, CURLOPT_PUT, 1 );
			curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, 'DELETE' );
		}

		$response = curl_exec( $curl );
		$err = curl_error( $curl );
		curl_close( $curl );

		if ( $err ) {
			return json_decode( $err );
		}

		$data = json_decode( $response );
		return $data;
	}

	/**
	 * Add Data to Airtable.
	 *
	 * @param  Array $data Data to be Added.
	 * @return Json
	 */
	function add_record( $data ) {

		if ( ! $data ) {
			return;
		}

		$data = array(
			'fields' => $data,
			);

		$data = json_encode( $data );
		return $this->airtable_call( $data, 'POST' );

	}

	/**
	 * Update Data to Airtable.
	 *
	 * @param  Array  $data      Data to be updated.
	 * @param  String $record_id ID of fileds where data to be update.
	 * @return Json
	 */
	function update_record( $data, $record_id ) {

		if ( ! $data ) {
			return;
		}

		$data = array(
			'fields' => $data,
		);

		$data = json_encode( $data );
		return $this->airtable_call( $data, 'PATCH', $record_id );
	}

	/**
	 * Delete Record.
	 *
	 * @param  String $record_id ID of fileds where data to be update.
	 * @return Json
	 */
	function delete_record( $record_id ) {

		if ( ! $record_id ) {
			return;
		}

		return $this->airtable_call( null, 'DELETE', $record_id );
	}
}
